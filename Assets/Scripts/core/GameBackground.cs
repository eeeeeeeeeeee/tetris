﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Core
{
    public class GameBackground : MonoBehaviour
    {
        public Texture background0;
        public Texture background1;
        
        private bool _dir = false;
        private float _maxDy = 3f;
        private float _dy = 0.2f;
        private float _maxScale, _minScale;
        private Material _material;

        private float _h = 0.3203f, _s = 0.3168f, _v = 0.6314f;
        public static Color Color;
        public static float Hue;
        public static int BackType;
        
        // Start is called before the first frame update
        void Start()
        {
            Renderer renderer = GetComponent<Renderer>();
            _material = renderer.material;
            var scale = transform.localScale.y;
            _maxScale = scale + _maxDy;
            _minScale = scale - _maxDy;

            Hue = 0.898f;
            BackType = 0;
        }

        // Update is called once per frame
        void FixedUpdate()
        {
            if (_dir)
            {
                if (transform.localScale.y < _maxScale)
                {
                    transform.localScale += new Vector3(0, _dy, 0);
                    transform.position += new Vector3(0, -0.05f, 0);
                }
                else
                {
                    _dir = false;
                }
            }
            else
            {
                if (transform.localScale.y > _minScale)
                {
                    transform.localScale += new Vector3(0, -_dy, 0);
                    transform.position += new Vector3(0, 0.05f, 0);
                }
                else
                {
                    _dir = true;
                }
            }

            // Color changing
            _h = Hue;
                
            Color = Color.HSVToRGB(_h, _s, _v);

            _material.SetColor("_Color", Color);
            
            // Changing texture
            if (BackType > 0)
            {
                _material.mainTexture = background1;
            }
            else
            {
                _material.mainTexture = background0;
            }
        }
    }
}