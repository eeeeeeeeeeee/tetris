﻿namespace Core
{
    public static class GameInfo
    {
        public static readonly int MaxScore;

        public static int ScorePoints { get; set; }
        public static int Level { get; set; }
        public static int MaxLevel { get; set; }
        public static int Score;

        public static void Reset()
        {
            Level = 1;
            MaxLevel = 3;
            ScorePoints = 0;
            Score = 0;
        }
        static GameInfo()
        {
            MaxScore = 100;
            Reset();
        }

    }
}