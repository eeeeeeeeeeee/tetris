﻿using Core.Entities;
using UnityEngine;

namespace Core
{
    public static class GameController
    {
        public const int LEFT = 0, RIGHT = 1, ROUND = 2, DOWN = 3;
        private static int[] _key = new int[4];

        public static void SetKey(int key, int value)
        {
            if(key>_key.Length-1) return;
            _key[key] = value;
        }
        public static void Controll(ref int[,] grid, Block block)
        {
            if (block == null) return;
            
            if (_key[LEFT] > 0)
            {
                _key[LEFT] = 0;
                block.Move(-1, 0, ref grid);
            }
            
            if (_key[RIGHT] > 0)
            {
                _key[RIGHT] = 0;
                block.Move(1, 0, ref grid);
            }
            
            if (_key[ROUND] > 0)
            {
                _key[ROUND] = 0;
                block.Rotate(true, ref grid);
            }
        }

        public static bool IsQuickDown()
        {
            if (_key[DOWN] > 0)
            {
                return true;
            }
            return false;
        }
    }
}