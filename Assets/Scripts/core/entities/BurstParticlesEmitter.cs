﻿using System;
using UnityEngine;

namespace Core.Entities
{
    public class BurstParticlesEmitter : MonoBehaviour
    {
        private bool _copied = true;
        private ParticleSystem _particleSystem;
        private static GameObject _burstParticlesObject = null;

        public static void InstantiateEmitter(Material material, Vector3 pos)
        {
            GameObject obj = Instantiate(_burstParticlesObject, pos, Quaternion.identity);
            ParticleSystemRenderer psr = obj.GetComponent<ParticleSystemRenderer>();
            psr.material = material;
        }
        private void Start()
        {
            _particleSystem = GetComponent<ParticleSystem>();
            if (_burstParticlesObject == null)
            {
                _copied = false;
                _burstParticlesObject = gameObject;
            }
        }

        private void Update()
        {
            if (_copied && _particleSystem.isStopped)
            {
                Destroy(gameObject);
            }
        }
    }
}