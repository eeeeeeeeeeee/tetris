﻿using System.Collections.Generic;
using Core;
using UnityEngine;

namespace Core.Entities
{
    public class Block
    {
        private List<Material> _materials;
        private Mesh _mesh;

        private int x, y;
        private int _color;
        private int _rotation;

        private SoundManager _soundManager;

        private byte[] _shape;
        public static readonly int MAXSIZE = 4;

        private readonly int W, H;

        private static readonly byte[][][] _shapeSet =
        {
            new[]
            {
                new byte[]
                {
                    0, 0, 0, 0,
                    0, 1, 1, 0,
                    0, 1, 1, 0,
                    0, 0, 0, 0
                }
            },
            new[]
            {
                new byte[]
                {
                    0, 0, 0, 0,
                    0, 0, 0, 0,
                    1, 1, 1, 1,
                    0, 0, 0, 0
                },
                new byte[]
                {
                    0, 0, 1, 0,
                    0, 0, 1, 0,
                    0, 0, 1, 0,
                    0, 0, 1, 0
                }
            },
            new[]
            {
                new byte[]
                {
                    0, 0, 0, 0,
                    0, 0, 1, 1,
                    0, 1, 1, 0,
                    0, 0, 0, 0
                },
                new byte[]
                {
                    0, 0, 1, 0,
                    0, 0, 1, 1,
                    0, 0, 0, 1,
                    0, 0, 0, 0
                }
            },
            new[]
            {
                new byte[]
                {
                    0, 0, 0, 0,
                    0, 1, 1, 0,
                    0, 0, 1, 1,
                    0, 0, 0, 0
                },
                new byte[]
                {
                    0, 0, 0, 1,
                    0, 0, 1, 1,
                    0, 0, 1, 0,
                    0, 0, 0, 0
                }
            },
            new[]
            {
                new byte[]
                {
                    0, 0, 0, 0,
                    0, 1, 1, 1,
                    0, 1, 0, 0,
                    0, 0, 0, 0
                },
                new byte[]
                {
                    0, 0, 1, 0,
                    0, 0, 1, 0,
                    0, 0, 1, 1,
                    0, 0, 0, 0
                },
                new byte[]
                {
                    0, 0, 0, 1,
                    0, 1, 1, 1,
                    0, 0, 0, 0,
                    0, 0, 0, 0
                },
                new byte[]
                {
                    0, 1, 1, 0,
                    0, 0, 1, 0,
                    0, 0, 1, 0,
                    0, 0, 0, 0
                }
            },
            new[]
            {
                new byte[]
                {
                    0, 0, 0, 0,
                    0, 1, 1, 1,
                    0, 0, 0, 1,
                    0, 0, 0, 0
                },
                new byte[]
                {
                    0, 0, 1, 1,
                    0, 0, 1, 0,
                    0, 0, 1, 0,
                    0, 0, 0, 0
                },
                new byte[]
                {
                    0, 1, 0, 0,
                    0, 1, 1, 1,
                    0, 0, 0, 0,
                    0, 0, 0, 0
                },
                new byte[]
                {
                    0, 0, 1, 0,
                    0, 0, 1, 0,
                    0, 1, 1, 0,
                    0, 0, 0, 0
                }
            },
            new[]
            {
                new byte[]
                {
                    0, 0, 0, 0,
                    0, 1, 1, 1,
                    0, 0, 1, 0,
                    0, 0, 0, 0
                },
                new byte[]
                {
                    0, 0, 1, 0,
                    0, 0, 1, 1,
                    0, 0, 1, 0,
                    0, 0, 0, 0
                },
                new byte[]
                {
                    0, 0, 1, 0,
                    0, 1, 1, 1,
                    0, 0, 0, 0,
                    0, 0, 0, 0
                },
                new byte[]
                {
                    0, 0, 1, 0,
                    0, 1, 1, 0,
                    0, 0, 1, 0,
                    0, 0, 0, 0
                }
            }
        };

        public Block(List<Material> materials, int color, Mesh mesh, SoundManager soundManager)
        {
            _soundManager = soundManager;
            _rotation = 0;

            _materials = materials;
            _mesh = mesh;
            _color = color;
            x = y = 0;

            W = GameField.W;
            H = GameField.H;

            _shape = new byte[MAXSIZE * MAXSIZE];
            _shape = _shapeSet[color][_rotation];
        }

        public bool Move(int dx, int dy, ref int[,] grid)
        {
            if (CheckPosition(x + dx, y + dy, ref grid))
            {
                _soundManager.Play(_soundManager.error);
                return true;
            }

            x += dx;
            y += dy;

            return false;
        }

        private bool CheckPosition(int x, int y, ref int[,] grid)
        {
            for (int i = 0; i < MAXSIZE; i++)
            {
                for (int j = 0; j < MAXSIZE; j++)
                {
                    if (_shape[j * MAXSIZE + i] < 1) continue;
                    if (x + i > W - 1 || x + i < 0) return true;
                    if (grid[x + i, y + j] > 0) return true;
                }
            }

            return false;
        }

        public void Draw(Vector3 zeroPos, ref int[,] grid)
        {
            Vector3 pos = zeroPos + new Vector3(x, -y, 0);
            Vector3 finPos = zeroPos + new Vector3(0, -18, 0);

            for (int i = y; i < H; i++)
            {
                if (CheckPosition(x, i, ref grid))
                {
                    finPos = zeroPos + new Vector3(x, -i + 1, 0);
                    break;
                }
            }

            for (int i = 0; i < MAXSIZE; i++)
            {
                for (int j = 0; j < MAXSIZE; j++)
                {
                    if (_shape[j * MAXSIZE + i] < 1) continue;
                    Vector3 delta = new Vector3(i, -j, 0);
                    Graphics.DrawMesh(_mesh, pos + delta, Quaternion.identity, _materials[_color + 1], 0);
                    Graphics.DrawMesh(_mesh, finPos + delta, Quaternion.identity, _materials[0], 0);
                }
            }
        }

        public void Rotate(bool dir, ref int[,] grid)
        {
            int prevRot = _rotation;
            int prevX = x;
            if (_rotation + 1 < _shapeSet[_color].Length)
            {
                _rotation += 1;
            }
            else
            {
                _rotation = 0;
            }

            _shape = _shapeSet[_color][_rotation];

            if (CheckPosition(x, y, ref grid))
            {
                if (!CheckPosition(x - 1, y, ref grid))
                {
                    x -= 1;
                    _soundManager.Play(_soundManager.rotate);
                    return;
                }

                if (!CheckPosition(x + 1, y, ref grid))
                {
                    x += 1;
                    _soundManager.Play(_soundManager.rotate);
                    return;
                }

                if (!CheckPosition(x + 2, y, ref grid))
                {
                    x += 2;
                    _soundManager.Play(_soundManager.rotate);
                    return;
                }

                _rotation = prevRot;
                _shape = _shapeSet[_color][_rotation];
            }
            else
            {
                _soundManager.Play(_soundManager.rotate);
            }
        }

        public static byte[] GetShape(int i)
        {
            return _shapeSet[i][0];
        }

        public void Burn(ref int[,] grid)
        {
            for (int i = 0; i < MAXSIZE; i++)
            {
                for (int j = 0; j < MAXSIZE; j++)
                {
                    if (_shape[j * MAXSIZE + i] < 1) continue;
                    grid[x + i, y + j] = _color + 1;
                }
            }
        }
    }
}