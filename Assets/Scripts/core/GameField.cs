﻿using System;
using System.Collections;
using System.Collections.Generic;
using Core.Entities;
using UnityEngine;
using Random = System.Random;

namespace Core
{

    public class GameField : MonoBehaviour
    {
        private float _time;
        private float _hold;
        private float _dt = 0.5f;
        private Mesh _mesh;
        private List<Material> _materials;

        private int[,] _grid;
        private bool _freezed;

        private float[,] _zgrid;
        private float _zstage = 0;

        public static readonly int W = 10, H = 21;
        private Block _block;
        private Vector3 _zeroPos;

        private int _next;
        private List<int> _removingLines;
        
        private SoundManager _soundManager;

        // Start is called before the first frame update
        void Start()
        {
            _soundManager = GetComponent<SoundManager>();
            
            MeshFilter meshFilter = GetComponent<MeshFilter>();
            _mesh = meshFilter.mesh;
            _time = Time.time;

            GenerateNext();

            _materials = new List<Material>();
            _materials.Add(Resources.Load<Material>("block alpha"));
            _materials.Add(Resources.Load<Material>("block blue"));
            _materials.Add(Resources.Load<Material>("block red"));
            _materials.Add(Resources.Load<Material>("block green"));
            _materials.Add(Resources.Load<Material>("block yellow"));
            _materials.Add(Resources.Load<Material>("block purple"));
            _materials.Add(Resources.Load<Material>("block dark blue"));
            _materials.Add(Resources.Load<Material>("block orange"));

            _grid = new int[W, H];
            _zeroPos = new Vector3(-4.5f, -1.5f);

            for (int i = 0; i < W; i++)
            {
                _grid[i, H - 1] = 1;
            }

            // Grid animation
            _zgrid = new float[W, H];
            for (int i = 0; i < W; i++)
            {
                for (int j = 0; j < H; j++)
                {
                    _zgrid[i, j] = UnityEngine.Random.Range(0f, (float) (2 * Math.PI));
                }
            }
            
            _removingLines = new List<int>();
        }

        private void GenerateNext()
        {
            _next = UnityEngine.Random.Range(0, 7);
            GUI.GuiBlock.ChangeNext(_next);
        }

        private void CallRemoveLine(int n)
        {
            _removingLines.Add(n);
        }

        private void FixedUpdate()
        {
            if (_freezed) return;
            
            // Call controlling method
            GameController.Controll(ref _grid, _block);
            if (GameController.IsQuickDown())
            {
                _dt = 0.04f;
            }
            else
            {
                _dt = 0.5f;
            }
            
            if (_zstage < 2 * Math.PI)
            {
                _zstage += 0.08f;
            }
            else _zstage = 0f;
        }

        private void RemoveLine(int line)
        {
            for (int i = line; i > 0; i--)
            {
                for (int j = 0; j < W; j++)
                {
                    _grid[j, i] = _grid[j, i-1];
                }
            }
        }
        private bool RemoveLines()
        {
            if (_removingLines.Count > 0)
            {
                if(_removingLines.Count > 3) GameBackground.BackType ^= 1;
                bool clean = false;
                foreach (var line in _removingLines)
                {
                    for (int i = 0; i < W / 2; i++)
                    {
                        if (_grid[W / 2 - i - 1, line] > 0)
                        {
                            // Burst particles
                            Vector3 pos0 = _zeroPos + new Vector3(W / 2 - i - 1, -line, 0);
                            Vector3 pos1 = _zeroPos + new Vector3(W / 2 + i, -line, 0);
                            BurstParticlesEmitter.InstantiateEmitter(_materials[_grid[W / 2 - i - 1, line]], pos0);
                            BurstParticlesEmitter.InstantiateEmitter(_materials[_grid[W / 2 + i, line]], pos1);
                            
                            _grid[W / 2 - i - 1, line] = 0;
                            _grid[W / 2 + i, line] = 0;
                            break;
                        }

                        if (i == W / 2 - 1)
                        {
                            clean = true;
                        }
                    }
                }

                if (clean)
                {
                    _removingLines.Sort();
                    foreach (var line in _removingLines)
                    {
                        RemoveLine(line);
                    }
                    
                    _removingLines.Clear();
                    GameBackground.BackType = 0;
                }
                return true;
            }

            return false;
        }
        
        // Update is called once per frame
        void Update()
        {
            if (_freezed) return;
            
            // Drawing grid of blocks
            for (int i = 0; i < W; i++)
            {
                for (int j = 0; j < H - 1; j++)
                {
                    if (_grid[i, j] < 1) continue;
                    Vector3 pos = _zeroPos + new Vector3(i, -j, (float) (0.13f * Math.Sin(_zstage + _zgrid[i, j])));
                    Graphics.DrawMesh(_mesh, pos, Quaternion.identity, _materials[_grid[i, j]], 0);
                }
            }

            // Removing lines
            if (RemoveLines())
            {
                return;
            }
            
            // Drawing block
            if (_block == null)
            {
                _block = new Block(_materials, _next, _mesh, _soundManager);
                if (_block.Move(0, 0, ref _grid))
                {
                    _freezed = true;
                    _soundManager.Play(_soundManager.failed);
                    GUI.GuiEndScreen.SetEnable();
                    return;
                }
                GenerateNext();
            }

            _block.Draw(_zeroPos, ref _grid);

            // Blocks moving
            if (Time.time < _time + _dt) return;
            _time = Time.time;
            if (_block.Move(0, 1, ref _grid))
            {
                _soundManager.Play(_soundManager.down);
                _block.Burn(ref _grid);
                _block = null;
            }

            for (int i = 0; i < H - 1; i++)
            {
                bool deleted = true;
                for (int j = 0; j < W; j++)
                {
                    if (_grid[j, i] < 1)
                    {
                        deleted = false;
                        break;
                    }
                }

                if (deleted)
                {
                    CallRemoveLine(i);
                    
                    // Score
                    GameInfo.Score += 10 * (GameInfo.MaxLevel - GameInfo.Level);
                    if (GameInfo.Score > GameInfo.MaxScore)
                    {
                        GameInfo.Score = 0;
                        GameInfo.Level += 1;
                    }
                }
            }

            if (_removingLines.Count > 0)
            {
                if (_removingLines.Count > 3)
                {
                    _soundManager.Play(_soundManager.destroy4);
                }
                else
                {
                    _soundManager.Play(_soundManager.destroy);
                }
            }
        }
    }
}