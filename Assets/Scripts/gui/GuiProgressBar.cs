﻿using System;
using UnityEngine;

namespace GUI
{
    public class GuiProgressBar : MonoBehaviour
    {
        private const int MaxProgress = 100;
        
        private SpriteRenderer _spriteRenderer;
        private float _width;
        private float _transformWidth;
        private Vector3 _position;
        private int _score;
        private void Start()
        {
            _spriteRenderer = GetComponent<SpriteRenderer>();
            _width = _spriteRenderer.size.x;
            _position = transform.position;

            _score = Core.GameInfo.Score;
            DisplayProgress(0);
        }

        private void DisplayProgress(int progress)
        {
            float newSize = _width * ((float)progress / MaxProgress);
            float dPercent = ((float) (MaxProgress - progress) / MaxProgress);
            _spriteRenderer.size = new Vector2((float)newSize, _spriteRenderer.size.y);
            transform.position = new Vector3(_position.x,_position.y,_position.z + dPercent*(_width+0.15f));
        }
        private void FixedUpdate()
        {
            if (Core.GameInfo.Score != _score)
            {
                _score = Core.GameInfo.Score;
                DisplayProgress(_score);
            }
        }
        
    }
}