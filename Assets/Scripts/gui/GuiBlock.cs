﻿using System;
using Core.Entities;
using UnityEngine;

namespace GUI
{
    public class GuiBlock : MonoBehaviour
    {
        private readonly int MAXSIZE = Block.MAXSIZE;
        private static int _next = 0;
        private static byte[] _shape;
        private Mesh _mesh;
        private Material _material;
        private Vector3 _deltaCube;

        private float _angle = 0f;

        // Start is called before the first frame update
        void Start()
        {
            MeshFilter meshFilter = GetComponent<MeshFilter>();
            _material = GetComponent<MeshRenderer>().material;
            _mesh = meshFilter.mesh;
            _shape = Block.GetShape(_next);
            _deltaCube = new Vector3(0, 0, 0.47f);
        }

        public static void ChangeNext(int i)
        {
            _next = i;
            _shape = Block.GetShape(_next);
            
            
            // Change background color
            switch (i)
            {
                case 0:
                    Core.GameBackground.Hue = 0.5692f;
                    break;
                case 1:
                    Core.GameBackground.Hue = 0.0f;
                    break;
                case 2:
                    Core.GameBackground.Hue = 0.3183f;
                    break;
                case 3:
                    Core.GameBackground.Hue = 0.1585f;
                    break;
                case 4:
                    Core.GameBackground.Hue = 0.8192f;
                    break;
                case 5:
                    Core.GameBackground.Hue = 0.6784f;
                    break;
                case 6:
                    Core.GameBackground.Hue = 0.1138f;
                    break;
            }
        }


        private void FixedUpdate()
        {
            if (_angle < Math.PI * 2)
            {
                _angle += 0.01f;
            }
            else
            {
                _angle = 0f;
            }
        }

        // Update is called once per frame
        void Update()
        {
            _material.SetColor("_Color", Core.GameBackground.Color);
            Vector3 dpos = Vector3.zero;
            if (_next < 2) dpos = _deltaCube;
            float dAngle = (float) (Math.Sin(_angle) * Math.PI / 6 + Math.PI / 2);

            for (int i = 0; i < MAXSIZE; i++)
            {
                for (int j = 0; j < MAXSIZE; j++)
                {
                    if (_shape[j * MAXSIZE + i] < 1) continue;
                    transform.rotation = Quaternion.Euler(0, -dAngle * 180 / (float) Math.PI, 0);
                    Matrix4x4 aX4 = transform.localToWorldMatrix;
                    aX4.m13 += j;
                    aX4.m03 += (i + dpos.z - MAXSIZE / 2) * (float) Math.Cos(dAngle);
                    aX4.m23 += (i + dpos.z - MAXSIZE / 2) * (float) Math.Sin(dAngle);
                    Graphics.DrawMesh(_mesh, aX4, _material, 0);
                }
            }
        }
    }
}