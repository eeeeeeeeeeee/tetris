﻿using System;
using System.Collections;
using System.Collections.Generic;
using Core;
using GUI;
using UnityEngine;

namespace GUI
{
    public class GuiControlButton : MonoBehaviour
    {
        public int Type;
        private SpriteRenderer _spriteRenderer;
        private Sprite[] _sprites;
        private Vector3 _globalPosition;

        private float _r;

        private KeyCode _controlButton;
        private bool _pressed;
        private bool _pressedBefore;
        
        private float _holdTime;
        private float _dt = 0.08f;
        
        // Start is called before the first frame update
        void Start()
        {
            _sprites = new Sprite[2];
            _spriteRenderer = gameObject.GetComponent<SpriteRenderer>();
            _sprites[0] = _spriteRenderer.sprite;
            _sprites[1] = Resources.Load<Sprite>("b_1");

            _r = (_spriteRenderer.sprite.bounds.size.x);
            
            CorrectPositionByType();
            
            switch (Type)
            {
                case GameController.LEFT:
                {
                    _controlButton = KeyCode.A;
                    break;
                }
                case GameController.RIGHT:
                {
                    _controlButton = KeyCode.D;
                    break;
                }
                case GameController.ROUND:
                {
                    _controlButton = KeyCode.Space;
                    _sprites[1] = Resources.Load<Sprite>("b_round_1");
                    _dt = 0.5f;
                    break;
                }
                case GameController.DOWN:
                {
                    _dt = 0.04f;
                    _controlButton = KeyCode.S;
                    break;
                }
            }
        }

        private void FixedUpdate()
        {
            if (_pressed)
            {
                if(_spriteRenderer.sprite != _sprites[1])
                    _spriteRenderer.sprite = _sprites[1];
            }
            else
            {
                if(_spriteRenderer.sprite != _sprites[0])
                    _spriteRenderer.sprite = _sprites[0];
            }
        }

        private void MouseDownRegister(Vector3 touchPos)
        {
            Vector3 pos = GuiCamera.Сamera.ScreenToWorldPoint(touchPos);
            var position = transform.position;
            float dz = pos.z - position.z;
            float dy = pos.y - position.y;
                
            if(dz*dz + dy*dy < _r*_r)
            {
                _pressed = true;
            }
        }

        private void MouseUpRegister()
        {
            _pressed = false;
        }

        // Update is called once per frame
        void Update()
        {
            // Send pressed keys
            if (!_pressedBefore && _pressed)
            {
                _holdTime = Time.time;
                GameController.SetKey(Type, 1);
            } else if (_pressed && _pressed == _pressedBefore)
            {
                if (Time.time > _holdTime + _dt)
                {
                    GameController.SetKey(Type, 4);
                    _holdTime = Time.time;
                }
            }

            if (_pressedBefore && !_pressed)
            {
                GameController.SetKey(Type, 0);
            }
            _pressedBefore = _pressed;
            
            // Get pressed keys
            if (Input.GetKeyDown(_controlButton))
            {
                _pressed = true;
            }
            else if (Input.GetKeyUp(_controlButton))
            {
                _pressed = false;
            }

            // Mouse processing
            if (Input.GetMouseButtonDown(0))
            {
                MouseDownRegister(Input.mousePosition);
            }

            if (Input.GetMouseButtonUp(0))
            {
                MouseUpRegister();
            }
            
        }
        
        private void CorrectPositionByType()
        {
            Vector3 pos;
            switch (Type)
            {
                case GameController.LEFT:
                    pos = GuiCamera.Сamera.ScreenToWorldPoint(new Vector3(0,0,0));
                    transform.position = new Vector3(transform.position.x, transform.position.y, pos.z-_r);
                    break;
                case GameController.RIGHT:
                    pos = GuiCamera.Сamera.ScreenToWorldPoint(new Vector3(0,0,0));
                    transform.position = new Vector3(transform.position.x, transform.position.y, pos.z-3*_r-0.1f);
                    break;
                case GameController.ROUND:
                    pos = GuiCamera.Сamera.ScreenToWorldPoint(
                        new Vector3(GuiCamera.Сamera.pixelRect.width,0,0));
                    transform.position = new Vector3(transform.position.x, transform.position.y, pos.z+_r);
                    break;
                case GameController.DOWN:
                    pos = GuiCamera.Сamera.ScreenToWorldPoint(
                        new Vector3(GuiCamera.Сamera.pixelRect.width,0,0));
                    transform.position = new Vector3(transform.position.x, transform.position.y, pos.z+3*_r+0.1f);
                    break;
            }
        }
    }

}