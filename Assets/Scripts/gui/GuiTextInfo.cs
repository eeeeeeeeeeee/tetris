﻿using System;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

namespace GUI
{
    public class GuiTextInfo : MonoBehaviour
    {
        private Text _text;
        private int _level;
        private int _maxLevel;
        private void Start()
        {
            _text = GetComponent<Text>();
            UpdateInfo();
        }

        private void UpdateInfo()
        {
            StringBuilder stringBuilder =  new StringBuilder();
            stringBuilder.Append(Core.GameInfo.Level);
            stringBuilder.Append(" | ");
            stringBuilder.Append(Core.GameInfo.MaxLevel);
            _text.text = stringBuilder.ToString();
        }
        private void FixedUpdate()
        {
            if (_level != Core.GameInfo.Level)
            {
                _level = Core.GameInfo.Level;
                UpdateInfo();
            }
        }
    }
}