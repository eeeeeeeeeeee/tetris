﻿using System;
using Core;
using UnityEngine;

namespace GUI
{
    public class GuiEndScreen : MonoBehaviour
    {
        private static Canvas _canvas;

        public static void SetEnable(bool val = true)
        {
            _canvas.enabled = val;
            if (val)
            {
                GameBackground.BackType = 1;
            }
            else
            {
                GameBackground.BackType = 0;
            }
        }
        private void Start()
        {
            _canvas = GetComponent<Canvas>();
            _canvas.enabled = false;
        }
    }
}