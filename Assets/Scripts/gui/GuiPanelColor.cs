﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace GUI
{
    public class GuiPanelColor : MonoBehaviour
    {
        public bool isText = false;
        private Material _material;
        private Text _text;

        // Start is called before the first frame update
        void Start()
        {
            if (isText)
            {
                _text = GetComponent<Text>();
                return;
            }

            try
            {
                Renderer render = GetComponent<Renderer>();
                _material = render.material;
            }
            catch (Exception e)
            {
                Image image = GetComponent<Image>();
                _material = image.material;
            }
        }

        // Update is called once per frame
        void Update()
        {
            if (isText)
            {
                _text.color = Core.GameBackground.Color;
                return;
            }

            _material.SetColor("_Color", Core.GameBackground.Color);
        }
    }
}