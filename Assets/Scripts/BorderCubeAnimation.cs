﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BorderCubeAnimation : MonoBehaviour
{
    private bool _dir;
    private bool _static;
    private float _dzMax = 0.13f;
    // Start is called before the first frame update
    void Start()
    {
        if (Random.Range(-10f, 10f) > 0) _dir = true;
        else _dir = false;
        if (Random.Range(0, 10f) > 9f) _static = true; 
        else _static = false;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if(_static) return;
        if (_dir)
        {
            if (gameObject.transform.position.z > -_dzMax)
            {
                gameObject.transform.position += new Vector3(0, 0,-0.005f);
            }
            else
            {
                _dir = false;
            }
        }
        else
        {
            if (gameObject.transform.position.z < _dzMax)
            {
                gameObject.transform.position += new Vector3(0, 0,0.005f);
            }
            else
            {
                _dir = true;
            }
        }
    }
}
