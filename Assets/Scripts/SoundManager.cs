﻿using System;
using UnityEngine;

public class SoundManager :  MonoBehaviour
{
    public AudioClip rotate;
    public AudioClip down;
    public AudioClip error;
    public AudioClip destroy;
    public AudioClip destroy4;
    public AudioClip failed;
    public AudioSource audioSource;

    public void Play(AudioClip sound)
    {
        audioSource.clip = sound;
        audioSource.Play();
    }
}
