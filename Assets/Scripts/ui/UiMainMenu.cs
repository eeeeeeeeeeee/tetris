﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Ui
{
    public class UiMainMenu : MonoBehaviour
    {
        private Canvas _canvas;
        private Canvas _welcomeCanvas;

        private void Start()
        {
            _canvas = GetComponent<Canvas>();
            _canvas.enabled = false;
            _welcomeCanvas = GameObject.Find("Welcome Canvas").GetComponent<Canvas>();
            _welcomeCanvas.enabled = true;
        }

        public void ShowSelf()
        {
            _canvas.enabled = true;
            _welcomeCanvas.enabled = false;
        }

        public void StartGame()
        {
            SceneManager.LoadScene("GameScene");
        }

        public void ExitGame()
        {
            Application.Quit();
        }
    }
}