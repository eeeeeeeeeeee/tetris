﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace Ui
{
    public class UiEndGame : MonoBehaviour
    {
        public void Restart()
        {
            Core.GameInfo.Reset();
            SceneManager.LoadScene("GameScene");
        }
        
        public void Exit()
        {
            Core.GameInfo.Reset();
            SceneManager.LoadScene("MainMenu");
        }
    }
}